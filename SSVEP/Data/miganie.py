#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 09:47:29 2019

@author: praceeg_1
"""

import SerialPort as SP
import time
import numpy as np

sp = SP.SerialPort('/dev/ttyUSB0')
sp.open()


sp.blinkSSVEP([0, 0],1,1)

A = np.array([4, 7, 10, 13, 16, 20, 25, 30, 35, 40])
X = A.copy()
T = np.zeros(2)
for i in range(14):
    X = np.concatenate((X,A))
A = X.copy()
f = 0
tstart = 0
while len(X) > 0:
    I = np.arange(0,len(X))
    s = (4)*np.random.sample()+3
    tstart += s
    time.sleep(s)
    idx2 = np.random.choice(I)
    while X[idx2] == f:
        idx2 = np.random.choice(I)
    f = X[idx2]
    X = np.delete(X, idx2)
    sp.blinkSSVEP([f, 0],1,1)
    time.sleep(5)
    sp.blinkSSVEP([0, 0],1,1)
    T = np.vstack((T,np.array([f,tstart])))
    print(len(X))
T[0,0] = 2
np.save('Seria2.npy',T)
sp.close()